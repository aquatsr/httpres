//
//  main.m
//  HTTPRes
//
//  Created by Tushar Rawat on 2/15/17.
//  Copyright (c) 2017 Tushar Rawat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
