//
//  AppDelegate.h
//  HTTPRes
//
//  Created by Tushar Rawat on 2/15/17.
//  Copyright (c) 2017 Tushar Rawat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate> 

@property (strong, nonatomic) UIWindow *window;


@end

