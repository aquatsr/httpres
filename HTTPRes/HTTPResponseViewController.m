//
//  HTTPResponseViewController.m
//  HTTPRes
//
//  Created by Tushar Rawat on 2/16/17.
//  Copyright (c) 2017 Tushar Rawat. All rights reserved.
//

#import "HTTPResponseViewController.h"

@implementation HTTPResponseViewController

@synthesize body, headerKeys, headerValues, tableView;

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [headerKeys count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:simpleTableIdentifier];
    }
    
    cell.textLabel.text = [headerKeys objectAtIndex:indexPath.row];
    cell.detailTextLabel.text = [headerValues objectAtIndex:indexPath.row];
    return cell;
}

@end
