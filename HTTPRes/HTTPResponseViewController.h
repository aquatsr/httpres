//
//  HTTPResponseViewController.h
//  HTTPRes
//
//  Created by Tushar Rawat on 2/16/17.
//  Copyright (c) 2017 Tushar Rawat. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HTTPResponseViewController : UITableViewController<UITableViewDelegate, UITableViewDataSource> {
    NSMutableData *body;
    NSArray *headerKeys;
    NSArray *headerValues;
}

@property (nonatomic, strong) NSMutableData *body;
@property (nonatomic, strong) NSArray *headerKeys;
@property (nonatomic, strong) NSArray *headerValues;

@end
