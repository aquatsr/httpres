//
//  ViewController.m
//  HTTPRes
//
//  Created by Tushar Rawat on 2/15/17.
//  Copyright (c) 2017 Tushar Rawat. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

@synthesize targetLabel, setTargetButton, clearTargetButton, launchButton, responseData, responseHeaders;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.targetLabel.text = @"";
    self.responseData = [[NSMutableData alloc] init];
}

- (IBAction)clearTarget:(id)sender {
    self.targetLabel.text = @"";
}

- (IBAction)setTargetEntry:(id)sender {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Enter Target" message:@"Enter a valid URI" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Done", nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    
    UITextField *alertTextField = [alert textFieldAtIndex:0];
    alertTextField.keyboardType = UIKeyboardTypeAlphabet;
    alertTextField.placeholder = @"Enter target URI";
    
    [alert show];
}

- (IBAction)launchButtonClicked:(id)sender {
    NSString *textCandidate = self.targetLabel.text;
    if (![textCandidate isEqual: @""]) {
        if ([textCandidate hasPrefix:@"http"]) {
            NSURL *urlStr = [NSURL URLWithString:textCandidate];
            NSURLRequest *urlRequest = [NSURLRequest requestWithURL:urlStr];
            NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:TRUE];
        }
    }
}

- (void)connection:(NSURLConnection*)connection didReceiveResponse:(NSURLResponse*)response {
    NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
    NSLog(@"%@", httpResponse);
    self.responseHeaders = httpResponse;
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [responseData appendData:data];
    NSString *dataStr = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    NSLog(@"%@", dataStr);
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    HTTPResponseViewController *responseViewController = [[HTTPResponseViewController alloc] initWithNibName:@"ResponseView" bundle:nil];
    NSDictionary *headerDict = [self.responseHeaders allHeaderFields];
    responseViewController.headerKeys = [headerDict allKeys];
    responseViewController.headerValues = [headerDict allValues];
    responseViewController.body = responseData;
    // [self presentViewController:responseViewController animated:YES completion:nil]; // modal view
    // we want to add to the navigation controller view stack
    [self.navigationController pushViewController:responseViewController animated:YES];
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    NSString *enteredText = [[alertView textFieldAtIndex:0] text];
    self.targetLabel.text = [NSString stringWithFormat:@"%@", enteredText];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
