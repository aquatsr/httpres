//
//  ViewController.h
//  HTTPRes
//
//  Created by Tushar Rawat on 2/15/17.
//  Copyright (c) 2017 Tushar Rawat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HTTPResponseViewController.h"

@interface ViewController : UIViewController<NSURLConnectionDelegate> {
    UIButton *setTargetButton;
    UIButton *clearTargetButton;
    IBOutlet UILabel *targetLabel;
    UIButton *launchButton;
    NSMutableData *responseData;
    NSHTTPURLResponse *responseHeaders;
}

@property (nonatomic, strong) UIButton *setTargetButton;
@property (nonatomic, strong) UIButton *clearTargetButton;
@property (nonatomic, strong) IBOutlet UILabel *targetLabel;
@property (nonatomic, strong) UIButton *launchButton;
@property (nonatomic, strong) NSMutableData *responseData;
@property (nonatomic, strong) NSHTTPURLResponse *responseHeaders;

- (IBAction)clearTarget:(id)sender;
- (IBAction)setTargetEntry:(id)sender;
- (IBAction)launchButtonClicked:(id)sender;

@end

